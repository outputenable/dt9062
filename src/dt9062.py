#!/usr/bin/env python3

#
# Copyright (c) 2020 Oliver Ebert <oe@outputenable.net>
#
# SPDX-License-Identifier: MIT
#

"""This module contains functions for reading and decoding serial data from
Digitek DT-9062 digital multimeters (DMMs).

- :func:`create_serial` creates a serial port (and requires the
  `pySerial <https://pypi.org/project/pyserial/>`_ package).

The byte stream transmitted by the multimeter consists of measurement records
of :data:`RECORD_LENGTH` bytes each.  Each byte of such a record contains a
counter value (the byte number, basically), from 1 to :data:`!RECORD_LENGTH`.

- :func:`read_record` tries to read just enough bytes required for a complete
  record.
- :func:`in_proper_sequence` returns the number of bytes in proper counter
  sequence for a record.
- :func:`sync` returns the number of leading data bytes that are *not* the
  start of a record.

- :func:`record_reader` is a generator yielding an infinite sequence of
  complete records.

A complete record is a sequence of bytes for which :func:`!in_proper_sequence`
returns :data:`!RECORD_LENGTH`; it can then be decoded:

- :func:`decode_measured_value` decodes the measured value including the unit
  prefix, if any.
- :func:`decode_unit` decodes the unit, if any.
- :func:`decode_mode` decodes various multimeter "modes" like AC, Delta,
  Autorange, etc. and returns corresponding :class:`Mode` enum flags.

In case of problems:

- :class:`Error` is the base class of all module-specific exceptions.
- :class:`IllegalEncoding` is raised when an illegal record encoding is
  detected.

Usage example
-------------

Let's suppose our multimeter is connected to ``/dev/ttyUSB0``.  After creating
the serial instance

>>> serial_port = create_serial("/dev/ttyUSB0")  # doctest: +SKIP

we can try to read a record:

>>> data = read_record(serial_port.read)  # doctest: +SKIP

(Since this is a doctest we won't actually do any I/O and instead supply some
canned sample data.)

>>> data = bytes([
...     0b0001_0100,
...     0b0010_0010,
...     0b1001_1000,
...     0b1010_0001,
...     0b1111_0010,
\
...     0b0001_0011,
...     0b0010_1111,
...     0b0011_1101,
...     0b0100_0001,
...     0b0101_1111,
...     0b0110_1001,
...     0b0111_1111,
...     0b1000_0111,
...     0b1001_1101,
... ])

Observe that the high nibble of each byte contains the record byte counter
(see :func:`in_proper_sequence`).

>>> n = in_proper_sequence(data)
>>> n
2
>>> len(data)
14

The number of bytes in proper sequence is not equal to :data:`RECORD_LENGTH`
and less than :code:`len(data)`, so apparently the data are somehow corrupted
(there's at least one "bad" data byte).  Skip the incomplete record and then
sync on the next possible start of a record:

>>> sync(data, n)
3
>>> data = data[n + sync(data, n) :]
>>> in_proper_sequence(data)
9
>>> len(data)
9

Now all the data are in proper sequence but the record is incomplete, so try
to read some more:

>>> data = read_record(serial_port.read, data)  # doctest: +SKIP

(Acting as if again.)

>>> data += bytes([
...     0b1010_0000,
...     0b1011_1000,
...     0b1100_0000,
...     0b1101_0100,
...     0b1110_0000,
... ])
>>> in_proper_sequence(data)
14
>>> RECORD_LENGTH
14

We found a complete record!

>>> decode_measured_value(data)
('-03.30', -3.3000000000000003, 'm', 0.001)
>>> decode_unit(data)
'V'
>>> decode_mode(data)
<Mode.RS232|AUTO>

Alright, on to the next one:

>>> data = data[RECORD_LENGTH:]
>>> n = in_proper_sequence(data)
>>> # and so forth

Fortunately, this module offers a :func:`record_reader` generator that deals
with all these complications internally and simply returns a sequence of
complete records ready to be decoded (discarding any corrupted data in the
process).  So a better way to go about this whole thing would probably be
something like

.. code-block::

   with create_serial("/dev/ttyUSB0") as serial_port:
       for record in record_reader(serial_port.read):
           # record is guaranteed to be a complete record (of `RECORD_LENGTH`
           # bytes).
           display, value, unit_prefix, factor = decode_measured_value(record)
           unit = decode_unit(record)
           mode = decode_mode(record)
           print(display, unit_prefix + unit, mode)

(That's also more or less what this module does when invoked as a standalone
script.)
"""

import enum
from typing import Callable, Iterable, Optional, Tuple, Union

serial = None

Read = Callable[[int], bytes]


class Error(Exception):
    pass


class IllegalEncoding(Error):
    def __init__(self, message: str, data: bytes):
        super().__init__(message)
        self.data = data


#
# Mapping from segment bit encoding to digits:
#
#     Byte n:   0bxxxx_x561
#     Byte n+1: 0bxxxx_4372
#
#      -1-
#     6   2
#      -7-
#     5   3
#      -4-
#
_DIGITS = {
    # n   n+1
    0b000_0000: (" ", float("nan")),
    0b000_0101: ("1", 1),
    0b001_0101: ("7", 7),
    0b001_1111: ("3", 3),
    0b010_0111: ("4", 4),
    0b011_1110: ("5", 5),
    0b011_1111: ("9", 9),
    0b101_1011: ("2", 2),
    # 'L', as part of " 0L " (for "0verLoad"; may also have a decimal point).
    0b110_1000: ("L", float("nan")),
    0b111_1101: ("0", 0),
    0b111_1110: ("6", 6),
    0b111_1111: ("8", 8),
}


def _decode_digit(byte1: int, byte2: int) -> Tuple[str, Union[int, float]]:
    segments = ((byte1 & 0b111) << 4) | (byte2 & 0b1111)
    try:
        return _DIGITS[segments]
    except KeyError:
        raise IllegalEncoding(
            "Invalid combination of segments", bytes((byte1, byte2))
        ) from None


RECORD_LENGTH = 14
"""The number of bytes in a complete record."""


_UNIT_PREFIXES = {
    0: ("", 1e0),
    # byte 10
    0b0010 << 4: ("k", 1e3),
    0b0100 << 4: ("n", 1e-9),
    0b1000 << 4: ("µ", 1e-6),
    # byte 11
    0b0010 << 0: ("M", 1e6),
    0b1000 << 0: ("m", 1e-3),
}


def _decode_unit_prefix(data: bytes, start: int = 0) -> Tuple[str, float]:
    key = ((data[start + 9] & 0b1110) << 4) | (data[start + 10] & 0b1010)
    try:
        return _UNIT_PREFIXES[key]
    except KeyError:
        raise IllegalEncoding(
            "More than one unit prefix", data[start : start + RECORD_LENGTH]
        ) from None


_DECIMAL_POINTS = {
    0b0000: ("", "", "", 1e0),
    0b0010: ("", "", ".", 1e-1),
    0b0100: ("", ".", "", 1e-2),
    0b1000: (".", "", "", 1e-3),
}


def decode_measured_value(data: bytes, start: int = 0) -> Tuple[str, float, str, float]:
    """Decode the measured value from the given complete record data.

    :param data: The data at *start* index shall be a complete record (of
       :data:`!RECORD_LENGTH` bytes :func:`in_proper_sequence`).
    :param start: The start index of the record in *data*.
    :returns: A 4-tuple with (in that order) the displayed string of
       (optional) sign and digits, its value (which may be NaN if e.g. " 0.L "
       is displayed), the unit prefix character (which may be empty), and the
       corresponding factor.
    :raises IndexError: If :code:`start + RECORD_LENGTH > len(data)` (that is
       the record is not complete) and a missing byte is accessed.
    :raises IllegalEncoding: If an illegal record encoding is detected,
       e.g. multiple decimal points.
    """
    byte2 = data[start + 1]
    sign = 1 - ((byte2 & 0b1000) >> 2)
    byte4 = data[start + 3]
    byte6 = data[start + 5]
    byte8 = data[start + 7]
    pos = (byte4 & 0b1000) | ((byte6 & 0b1000) >> 1) | ((byte8 & 0b1000) >> 2)
    try:
        bp, cp, dp, multiplier = _DECIMAL_POINTS[pos]
    except KeyError:
        raise IllegalEncoding(
            "More than one decimal point", data[start : start + RECORD_LENGTH]
        ) from None
    try:
        # display, value
        ad, av = _decode_digit(byte2, data[start + 2])
        bd, bv = _decode_digit(byte4, data[start + 4])
        cd, cv = _decode_digit(byte6, data[start + 6])
        dd, dv = _decode_digit(byte8, data[start + 8])
    except IllegalEncoding:
        raise IllegalEncoding("Invalid digit", data[start : start + RECORD_LENGTH])
    display = "".join(((None, " ", "-")[sign], ad, bp, bd, cp, cd, dp, dd))
    value = sign * (av * 1000 + bv * 100 + cv * 10 + dv) * multiplier
    unit_prefix, factor = _decode_unit_prefix(data, start)
    return display, value, unit_prefix, factor


_UNITS = {
    0: "",
    # byte 11
    0b0100 << 12: "%",
    # byte 12
    0b0100 << 8: "Ω",
    0b1000 << 8: "F",
    # byte 13
    0b0010 << 4: "Hz",
    0b0100 << 4: "V",
    0b1000 << 4: "A",
    # byte 14
    # There is U+00B0 DEGREE SIGN and U+2103 DEGREE CELSIUS; we're using the
    # former here.
    0b0100 << 0: "°C",
}


def decode_unit(data: bytes, start: int = 0) -> str:
    """Decode the unit of measurement from the given complete record data.

    :param data: The data at *start* index shall be a complete record (of
       :data:`!RECORD_LENGTH` bytes :func:`in_proper_sequence`).
    :param start: The start index of the record in *data*.
    :returns: Unit of measurement; may be empty.
    :raises IndexError: If :code:`start + RECORD_LENGTH > len(data)` (that is
       the record is not complete) and a missing byte is accessed.
    :raises IllegalEncoding: If the record specifies more than one unit.
    """
    key = (
        ((data[start + 10] & 0b0100) << 12)
        | ((data[start + 11] & 0b1100) << 8)
        | ((data[start + 12] & 0b1110) << 4)
        | (data[start + 13] & 0b0100)
    )
    try:
        return _UNITS[key]
    except KeyError:
        units = ", ".join(unit for bit, unit in _UNITS.items() if bit & key)
        raise IllegalEncoding(
            f"More than one unit: {units}", data[start : start + RECORD_LENGTH]
        ) from None


def _is_more_than_one_bit_set(value: int) -> bool:
    # http://graphics.stanford.edu/~seander/bithacks.html#DetermineIfPowerOf2
    return (value & (value - 1)) != 0


class Mode(enum.Flag):
    NONE = 0
    AC = enum.auto()
    DELTA = enum.auto()
    AUTO = enum.auto()
    CONTINUITY = enum.auto()  # "Beep"
    DIODE = enum.auto()
    BATTERY_LOW = enum.auto()
    RS232 = enum.auto()

    def __repr__(self):
        # Hide the (unimportant) value as suggested in
        # https://docs.python.org/3/library/enum.html
        if not _is_more_than_one_bit_set(self.value):
            names = self.name
        else:
            names = "|".join(
                member.name
                for member in reversed(self.__class__)
                if self.value & member.value
            )
        return "".join(("<", self.__class__.__name__, ".", names, ">"))


_MODE_SPECS = (
    (0, 0b0001, Mode.RS232),
    (0, 0b0010, Mode.AUTO),
    (0, 0b1000, Mode.AC),
    (9, 0b0001, Mode.DIODE),
    (10, 0b0001, Mode.CONTINUITY),
    (11, 0b0010, Mode.DELTA),
    (12, 0b0001, Mode.BATTERY_LOW),
)


def decode_mode(data: bytes, start: int = 0) -> Mode:
    """Decode multimeter mode.

    :param data: The data at *start* index shall be a complete record (of
       :data:`!RECORD_LENGTH` bytes :func:`in_proper_sequence`).
    :param start: The start index of the record in *data*.
    :returns: Combination of :class:`Mode` members that are selected/active.
    :raises IndexError: If :code:`start + RECORD_LENGTH > len(data)` (that is
       the record is not complete) and a missing byte is accessed.
    :raises IllegalEncoding: If an illegal record encoding is detected.
    """
    mode = Mode.NONE
    for index, bit, member in _MODE_SPECS:
        if data[start + index] & bit:
            mode |= member
    return mode


def sync(data: bytes, start: int = 0) -> int:
    """Return the number of leading bytes that *cannot* start a measurement
    record.

    :param data: Bytes containing (presumably) measurement record data.
    :param start: Start index in *data*; may be greater than or equal to
       :code:`len(data)`.
    :returns: The number of bytes (at *start* index) that *cannot* start a
       measurement record (that do not have a counter value of 1).  The next
       eligible *data* index for the start of a new record is thus
       :code:`start + sync(data, start)`---which may be equal to
       :code:`len(data)`!  Use :func:`in_proper_sequence` to check the data at
       that index.
    """
    index = start
    length = len(data)
    while index < length and (data[index] & 0xF0) != (1 << 4):
        index += 1
    return index - start


def in_proper_sequence(data: bytes, start: int = 0) -> int:
    """Return the number of bytes in proper counter sequence for a measurement
    record.

    Each byte transmitted by the multimeter contains (or is supposed to, at
    least) a "counter" value, which is the byte number inside the measurement
    record, starting at 1.

    :param data: Bytes containing (presumably) measurement record data.
    :param start: Start index in *data*; may be greater than or equal to
       :code:`len(data)`.
    :returns: The number of bytes (at *start* index) in proper counter
       sequence for a measurement record.  The returned value is in the range
       0 .. :data:`RECORD_LENGTH` (inclusive).  A value of
       :data:`!RECORD_LENGTH` indicates a complete record that is ready to be
       decoded.
    """
    expected_counter = 1 << 4
    for byte in data[start : start + RECORD_LENGTH]:
        if (byte & 0xF0) != expected_counter:
            break
        expected_counter += 1 << 4
    return (expected_counter >> 4) - 1


def create_serial(port: Optional[str], timeout: Optional[float] = None):
    global serial
    if serial is None:
        import serial

    return serial.Serial(
        port=port,
        baudrate=2400,
        bytesize=serial.EIGHTBITS,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        timeout=timeout,
    )


def read_record(read: Read, partial: Optional[bytes] = None) -> bytes:
    """Read just enough bytes to make a complete record.

    Read up to :data:`RECORD_LENGTH` bytes missing to make *partial* a
    complete record.  Note that this does *not* check whether the resulting
    data are still :func:`in_proper_sequence`!

    :param read: Callable that takes the number of bytes to read and returns
       bytes read (which may be fewer than requested).
    :param partial: If provided then :code:`len(partial)` must be
       :func:`in_proper_sequence`.
    :returns: *partial* (if any) + the bytes returned by *read()*.
    :raises: Anything that *read* raises.
    """
    n = RECORD_LENGTH - len(partial) if partial else RECORD_LENGTH
    data = read(n)
    return partial + data if partial else data


def record_reader(read: Read, max_retries: int = 3) -> Iterable[bytes]:
    data = None
    retry = 0
    while True:
        data = read_record(read, data)
        while True:
            n = in_proper_sequence(data)
            if n == RECORD_LENGTH:
                # Got a complete record.
                yield data
                data = None
                retry = 0
                break
            elif n == len(data):
                # OK so far but need more data.
                if retry < max_retries:
                    retry += 1
                    break
                else:
                    return
            else:
                # Bad data; discard everything up to the next possible start
                # of a record.
                data = data[n + sync(data, n) :]


def main():
    import argparse
    import datetime

    parser = argparse.ArgumentParser()
    parser.add_argument("port", help="The serial port/device to use.")
    parser.add_argument(
        "--strftime",
        metavar="FORMAT",
        default="%H:%M:%S.%f",
        help="Format string for `strftime`; defaults to '%(default)s'.",
    )

    args = parser.parse_args()

    with create_serial(args.port) as serial_port:
        for record in record_reader(serial_port.read):
            timestamp = datetime.datetime.now().strftime(args.strftime)
            try:
                display, _, unit_prefix, _ = decode_measured_value(record)
                unit = decode_unit(record)
            except Error as e:
                print(timestamp, e.__class__.__name__ + ":", e)
            else:
                print(timestamp, display, unit_prefix + unit)


def interruptible_main():
    try:
        main()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    interruptible_main()
