import math
import unittest

import dt9062


class DecodeDigitTest(unittest.TestCase):

    NUMERICAL_DIGITS = (
        ((0b111, 0b1101), ("0", 0)),
        ((0b000, 0b0101), ("1", 1)),
        ((0b101, 0b1011), ("2", 2)),
        ((0b001, 0b1111), ("3", 3)),
        ((0b010, 0b0111), ("4", 4)),
        ((0b011, 0b1110), ("5", 5)),
        ((0b111, 0b1110), ("6", 6)),
        ((0b001, 0b0101), ("7", 7)),
        ((0b111, 0b1111), ("8", 8)),
        ((0b011, 0b1111), ("9", 9)),
    )

    BLANK_DIGIT = ((0b000, 0b0000),)
    L_DIGIT = ((0b110, 0b1000),)

    def test_numerical_digits(self):
        for digit in DecodeDigitTest.NUMERICAL_DIGITS:
            with self.subTest(digit=digit):
                self.assertEqual(dt9062._decode_digit(*digit[0]), digit[1])

    def test_blank_digit(self):
        display, value = dt9062._decode_digit(*DecodeDigitTest.BLANK_DIGIT[0])
        self.assertEqual(display, " ")
        self.assertNotEqual(value, value)  # NaN?
        self.assertTrue(math.isnan(value))  # double-check

    def test_L_digit(self):
        display, value = dt9062._decode_digit(*DecodeDigitTest.L_DIGIT[0])
        self.assertEqual(display, "L")
        self.assertNotEqual(value, value)  # NaN?
        self.assertTrue(math.isnan(value))  # double-check

    VALID_SEGMENT_ENCODINGS = tuple(
        (d[0][0] << 4) | d[0][1] for d in NUMERICAL_DIGITS + (BLANK_DIGIT,) + (L_DIGIT,)
    )

    def test_illegal_encodings(self):
        for segments in range(0b111_1111 + 1):
            if segments not in DecodeDigitTest.VALID_SEGMENT_ENCODINGS:
                byte1 = segments >> 4
                byte2 = segments & 0b1111
                with self.subTest(byte1=byte1, byte2=byte2):
                    self.assertRaises(
                        dt9062.IllegalEncoding, dt9062._decode_digit, byte1, byte2
                    )
