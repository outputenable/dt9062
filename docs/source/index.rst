.. dt9062 documentation master file, created by
   sphinx-quickstart on Sat May  9 19:30:09 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dt9062's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dt9062

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
