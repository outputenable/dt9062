import setuptools

setuptools.setup(
    name="dt9062",
    version="0.1.0",
    author="Oliver Ebert",
    author_email="oe@outputenable.net",
    description="A module for reading and decoding serial data from Digitek DT-9062 digital multimeters (DMMs)",
    keywords=["digitek", "multimeter", "dmm", "dt-9062", "serial", "rs-232"],
    url="https://gitlab.com/outputenable/dt9062",
    command_options={
        "build_sphinx": {
            "source_dir": ("setup.py", "docs/source"),
            "build_dir": ("setup.py", "docs/build"),
        }
    },
    package_dir={"": "src"},
    py_modules=["dt9062"],
    python_requires="~=3.6",
    install_requires=["pyserial~=3.0"],
    entry_points={"console_scripts": ["dt9062 = dt9062:interruptible_main"]},
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.7",
        "Topic :: Scientific/Engineering",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Terminals :: Serial",
    ],
)
