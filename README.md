dt9062
======

A Python module for reading and decoding serial data from Digitek DT-9062
digital multimeters (DMMs).

* [Project home page](https://gitlab.com/outputenable/dt9062)
* [Documentation](https://dt9062.readthedocs.io/)

Most of the info is gleaned from this page (German only I'm afraid):
[Das Vielfach-Messinstrument DT-9062 mit RS-232 Schnittstelle](https://web.archive.org/web/20140614001104/http://www.physiktreff.de/material/digitek/digitek.htm)

Installation
------------

    $ pipenv install -e git+https://gitlab.com/outputenable/dt9062.git#egg=dt9062

Development
-----------

    $ git clone https://gitlab.com/outputenable/dt9062.git
    $ cd dt9062
    $ pipenv install
    $ pipenv shell

    $ python setup.py build_sphinx  # build Sphinx documentation
    $ python -m doctest [-v] src/dt9062.py  # run doctests
    $ python -m unittest discover -s src  # run unit tests
    $ python -m mypy src/dt9062.py  # check type annotations
